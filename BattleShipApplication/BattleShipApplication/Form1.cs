﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace BattleShipApplication
{
    public partial class Form1 : Form
    {
        
        byte[] data = new byte[1024];
        int recv;
        //WaitForm wf;
        string input, stringData;
        IPAddress host;
        IPEndPoint ipep;
        const int RowCount = 10;
        const int ColumnCount = 10;
        //List<Label> ownField;
        Label[,] ownCourt;
        Label[,] enemyCourt;
        Label[] ships;
        Label[,] AICourt;
        bool playerIsShooted = false;
        int lightblueCount = 0;
        CoreAI nai;
        bool AIIsAlreadyShooted = false;
        int ownSunkShipCounter = 0;
        int AISunkShipCounter = 0;




        public Form1()
        {
            InitializeComponent();
            host = IPAddress.Parse(GetLocalIPAddress());
            ipep = new IPEndPoint(host, 3000);
            connectionProperties.Hide();
            serverAddress.Hide();
            lbServerAddress.Text = ipep.Address + ":" + ipep.Port;
            //ownField = new List<Label>();
            //var labelarray = new Label[RowCount, ColumnCount];
            ownCourt = new Label[RowCount, ColumnCount];
            enemyCourt = new Label[RowCount, ColumnCount];
            AICourt = new Label[RowCount, ColumnCount];
            

            /**************First field draw**************/
            for (int i = 0; i < RowCount; i++)
            {
                Label colSequence = new Label();
                colSequence.Size = new Size(20, 20);
                colSequence.Location = new Point(100 + (i * 20), 40);
                colSequence.Text = (i + 1).ToString();
                this.Controls.Add(colSequence);
               
                for (int j = 0; j < ColumnCount; j++)
                {
                    //Add row sequence
                    Label rowSequence = new Label();
                    rowSequence.Size = new Size(20, 20);
                    rowSequence.Location = new Point(80, 63 + (j * 20)); //kicsit lejjeb kell rakni a label keretek miatt
                    rowSequence.Text = (j + 1).ToString();
                    this.Controls.Add(rowSequence);

                    Label lbl = new Label();
                    lbl.Size = new Size(20, 20);
                    lbl.Location = new Point(100 + (i * 20), 60 + (j * 20));
                    //lbl.BackColor = (i + j) % 2 == 0 ? Color.White : Color.White;
                    lbl.BackColor = Color.LightBlue;
                    lbl.TextAlign = ContentAlignment.MiddleCenter; // text align
                    lbl.BorderStyle = BorderStyle.FixedSingle;
                    lbl.DragDrop += lbl_DragDrop;
                    lbl.AllowDrop = true;
                    lbl.DragEnter += lbl_DragEnter;
                    this.Controls.Add(lbl);
                    //ownField.Add(lbl);
                    ownCourt[i, j] = lbl;
                }
            }
            /**************Second field draw*************/
            for (int i = 0; i < RowCount; i++)
            {
                Label sequence = new Label();
                sequence.Size = new Size(20, 20);
                sequence.Location = new Point(600 + (i * 20), 40);
                sequence.Text = (i + 1).ToString();
                this.Controls.Add(sequence);

                for (int j = 0; j < ColumnCount; j++)
                {
                    //Add row sequence
                    Label rowSeq = new Label();
                    rowSeq.Size = new Size(20, 20);
                    rowSeq.Location = new Point(580, 63 + (j * 20)); //kicsit lejjeb kell rakni a label keretek miatt
                    rowSeq.Text = (j + 1).ToString();
                    this.Controls.Add(rowSeq);

                    Label lbl = new Label();
                    lbl.Size = new Size(20, 20);
                    lbl.Location = new Point(600 + (i * 20), 60 + (j * 20));
                    //lbl.BackColor = (i + j) % 2 == 0 ? Color.White : Color.White;
                    lbl.BackColor = Color.LightBlue;
                    lbl.TextAlign = ContentAlignment.MiddleCenter; // text align
                    lbl.BorderStyle = BorderStyle.FixedSingle;
                    lbl.Click += lbl_Click;
                    this.Controls.Add(lbl);
                    enemyCourt[i, j] = lbl;
                }
            }
            //ownField[0].BackColor = Color.Green;
            //ownCourt[1,1].BackColor = Color.GreenYellow;
            //enemyCourt[5, 8].BackColor = Color.DarkRed;

            //Hajók alaphelyzetű elhelyezése


            ships = new Label[5];
            //Hajok felhelyezese
            for(int i = 0; i < 5; i++)
            {
                Label tmpLabel = new Label();
                tmpLabel.Height = 20;
                tmpLabel.BorderStyle = BorderStyle.FixedSingle;
                if (i < 1)
                {
                    tmpLabel.Width = (i + 2) * 20;
                } else
                {
                    tmpLabel.Width = (i + 1) * 20;
                }
                tmpLabel.BackColor = Color.Black;
                tmpLabel.MouseDown += TmpLabel_MouseDown;
                tmpLabel.Location = new Point(40, 300 + (i * 20));
                this.Controls.Add(tmpLabel);
                tmpLabel.Visible = false;
                ships[i] = tmpLabel;

            }

           /* ownCourt[0, 0].BackColor = Color.Black;
            ownCourt[0, 1].BackColor = Color.Black;
            ownCourt[0, 2].BackColor = Color.Black;

            ownCourt[2, 3].BackColor = Color.Black;
            ownCourt[3, 3].BackColor = Color.Black;
            ownCourt[4, 3].BackColor = Color.Black;
            ownCourt[5, 3].BackColor = Color.Black;*/
            


        }

        void setShipsToDefault()
        {
            ships = new Label[5];
            //Hajok felhelyezese
            for (int i = 0; i < 5; i++)
            {
                Label tmpLabel = new Label();
                tmpLabel.Height = 20;
                tmpLabel.BorderStyle = BorderStyle.FixedSingle;
                if (i < 1)
                {
                    tmpLabel.Width = (i + 2) * 20;
                }
                else
                {
                    tmpLabel.Width = (i + 1) * 20;
                }
                tmpLabel.BackColor = Color.Black;
                tmpLabel.MouseDown += TmpLabel_MouseDown;
                tmpLabel.Location = new Point(40, 300 + (i * 20));
                this.Controls.Add(tmpLabel);
                ships[i] = tmpLabel;

            }
        }

        private void TmpLabel_MouseDown(object sender, MouseEventArgs e)
        {
            Label lbl = sender as Label;
            if (lbl != null)
            {
                lbl.DoDragDrop((lbl.Width / 20), DragDropEffects.Move);
            }
        }

        private void lbl_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }


        void initAIShips (int _shipSize)
        {
            int shipSize = _shipSize;
            int k = 1;
            bool aiHaveBlack = false;
            bool needNextTry = true;
            int aiShipStartPosI = 0;
            int aiShipStartPosJ = 8;
            int isHorizontal = 0;
            while (needNextTry)
            {
                aiHaveBlack = false;
                isHorizontal = (int)(new Random().NextDouble() * 10) % 2;
                aiShipStartPosI = ((int)(new Random().NextDouble() * RowCount) * 63) % 10;
                aiShipStartPosJ = ((int)(new Random().NextDouble() * ColumnCount) * (17 + aiShipStartPosI)) % 10;
                if (isHorizontal == 1)
                {
                    if ((shipSize + aiShipStartPosI) < RowCount)
                    {
                        if (!(AICourt[aiShipStartPosI, aiShipStartPosJ].BackColor == Color.Black))
                        {
                            k = 1;
                            while (k < shipSize)
                            {
                                if (AICourt[aiShipStartPosI + k, aiShipStartPosJ].BackColor == Color.Black)
                                {
                                    aiHaveBlack = true;
                                }
                                k++;
                            }
                        }
                        else
                        {
                            aiHaveBlack = true;
                        }

                        if (!aiHaveBlack)
                        {
                            k = 1;
                            AICourt[aiShipStartPosI, aiShipStartPosJ].BackColor = Color.Black; //AI ugyanoda teszi a hajókat ahova a player
                            while (k < shipSize)
                            {
                                AICourt[aiShipStartPosI + k, aiShipStartPosJ].BackColor = Color.Black; //AI ugyanoda teszi a hajókat ahova a player
                                k++;
                            }
                            needNextTry = false;
                        }
                    }
                }
                else {
                    if ((shipSize + aiShipStartPosJ) < RowCount)
                    {
                        if (!(AICourt[aiShipStartPosI, aiShipStartPosJ].BackColor == Color.Black))
                        {
                            k = 1;
                            while (k < shipSize)
                            {
                                if (AICourt[aiShipStartPosI, aiShipStartPosJ + k].BackColor == Color.Black)
                                {
                                    aiHaveBlack = true;
                                }
                                k++;
                            }
                        }
                        else
                        {
                            aiHaveBlack = true;
                        }

                        if (!aiHaveBlack)
                        {
                            k = 1;
                            AICourt[aiShipStartPosI, aiShipStartPosJ].BackColor = Color.Black; //AI ugyanoda teszi a hajókat ahova a player
                            while (k < shipSize)
                            {
                                AICourt[aiShipStartPosI, aiShipStartPosJ + k].BackColor = Color.Black; //AI ugyanoda teszi a hajókat ahova a player
                                k++;
                            }
                            needNextTry = false;
                        }
                    }
                }
            }
        }



        void lbl_DragDrop(object sender, DragEventArgs e)
        {
            Label lbl = (Label)sender;
            int shipSize = (int)e.Data.GetData(typeof(int));
            int k = 1;
            bool ownHaveBlack = false;
            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < ColumnCount; j++)
                {
                    if (lbl.Location == ownCourt[i, j].Location)
                    {
                        if (MessageBox.Show("Do you want to place it Horizontal?", "Placement", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            if ((shipSize + i) <= RowCount)
                            {             
                                //van-e fekete ott, ahova fel akarom rakni a hajót?                   
                                if (!(((Label)sender).BackColor == Color.Black))
                                {
                                    k = 1;
                                    while (k < shipSize)
                                    {
                                        if (ownCourt[i + k, j].BackColor == Color.Black)
                                        {
                                            ownHaveBlack = true;
                                        }
                                        k++;
                                    }
                                } else
                                {
                                    ownHaveBlack = true;
                                }
                                

                                if (!ownHaveBlack)
                                {
                                    k = 1;
                                    ((Label)sender).BackColor = Color.Black;
                                    while (k < shipSize)
                                    {
                                        ownCourt[i + k, j].BackColor = Color.Black;
                                        k++;
                                    }

                                    initAIShips(shipSize);

                                    int l = 0;
                                    //felrakott hajó levétele, mint felhelyezhető hajó
                                    while (l < ships.Length)
                                    {
                                        if ((ships[l].Width / 20) == shipSize)
                                        {
                                            ships[l].Width = 0;
                                            ships[l].Height = 0;
                                            l = ships.Length;
                                        }
                                        l++;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if ((shipSize + j) <= ColumnCount)
                            {
                                if (!(((Label)sender).BackColor == Color.Black))
                                {
                                    k = 1;
                                    while (k < shipSize)
                                    {
                                        if (ownCourt[i, j + k].BackColor == Color.Black)
                                        {
                                            ownHaveBlack = true;
                                        }
                                        k++;
                                    }
                                }
                                else
                                {
                                    ownHaveBlack = true;
                                }
                                if (!ownHaveBlack)
                                {
                                    k = 1;
                                    ((Label)sender).BackColor = Color.Black;
                                    while (k < shipSize)
                                    {
                                        ownCourt[i, j + k].BackColor = Color.Black;
                                        k++;
                                    }
                                    initAIShips(shipSize);
                                    int l = 0;
                                    while (l < ships.Length)
                                    {
                                        if ((ships[l].Width / 20) == shipSize)
                                        {
                                            ships[l].Width = 0;
                                            ships[l].Height = 0;
                                            l = ships.Length;
                                        }
                                        l++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void hostServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            serverAddress.Show();
            Thread server = new Thread(new ThreadStart(startServer));
            server.IsBackground = true;
            server.Start();
            
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        private void startServer()
        {
            
            Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                listener.Bind(ipep);
            }
            catch (SocketException soex)
            {
                Console.WriteLine("Unable to create server.");
                Console.WriteLine(soex.ToString());
                return;
            }
            listener.Listen(10);
            Console.WriteLine("Waiting for the client...");
            Console.WriteLine("On: "+ipep.Address + ":" + ipep.Port);

            Socket client = listener.Accept();
            IPEndPoint clientep = (IPEndPoint)client.RemoteEndPoint;
            //serverAddress.Hide();
            Console.WriteLine("Connected with {0} at port {1}", clientep.Address, clientep.Port);

            string welcome = "Welcome to my test server";
            data = Encoding.ASCII.GetBytes(welcome);
            client.Send(data, data.Length, SocketFlags.None);

            while (true)
            {
                data = new byte[1024];
                recv = client.Receive(data);
                if (recv == 0)
                    break;

                Console.WriteLine(
                         Encoding.ASCII.GetString(data, 0, recv));
                client.Send(data, recv, SocketFlags.None);
            }
            Console.WriteLine("Disconnected from {0}", clientep.Address);
            client.Close();
            listener.Close();

        }

        private void btnJoin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbIPAddress.Text) || string.IsNullOrWhiteSpace(tbPort.Text))
            {
                MessageBox.Show("IP and port can't be empty!");
            }else
            {
                connectionProperties.Hide();
                IPEndPoint ipep = new IPEndPoint(IPAddress.Parse(tbIPAddress.Text), Int32.Parse(tbPort.Text));
                Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                try
                {
                    server.Connect(ipep);
                }
                catch (SocketException ex)
                {
                    MessageBox.Show("Unable to connect to server.");
                    Console.WriteLine("Unable to connect to server.");
                    Console.WriteLine(ex.ToString());
                    return;
                }

                int recv = server.Receive(data);
                stringData = Encoding.ASCII.GetString(data, 0, recv);
                Console.WriteLine(stringData);

                while (true)
                {
                    input = Console.ReadLine();
                    if (input == null)
                        break;
                    server.Send(Encoding.ASCII.GetBytes("hello server"));
                    data = new byte[1024];
                    recv = server.Receive(data);
                    stringData = Encoding.ASCII.GetString(data, 0, recv);
                    Console.WriteLine(stringData);
                }
                Console.WriteLine("Disconnecting from server...");
                server.Shutdown(SocketShutdown.Both);
                server.Close();
            }
        }

        private void joinToServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            connectionProperties.Show();
        }

        /********************Palya Config**********************/
        
        private void button1_Click(System.Object sender, System.EventArgs e)
        {
            
        }

        private int CountCellsOfColor(Color color)
        {
            int count = 0;
            foreach (Label lbl in this.Controls.OfType<Label>())
            {
                if (lbl.BackColor == color) count += 1;
            }
            return count;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            

            //MessageBox.Show(CountCellsOfColor(Color.Black).ToString());
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            initializeAICourt();
            nai = new CoreAI();
            for (int i = 0; i < ships.Length; i++)
            {
                ships[i].Visible = true;
            }
            MessageBox.Show("A játék megkezdéséhez, helyezd el a hajóidat a pályán, majd nyomj a Mehet! gombra.");
        }

        private void initializeAICourt()
        {
            /**************AI court label matrix létrehozása:(ha az enemy court-re lövünk akkor majd ezzel kell összehasonlítani, h vol e talalat)**************/
            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < ColumnCount; j++)
                {
                    Label lbl = new Label();
                    lbl.Size = new Size(20, 20);
                    lbl.Location = new Point(100 + (i * 20), 60 + (j * 20));
                    lbl.BackColor = Color.LightBlue;
                    AICourt[i, j] = lbl;
                }
            }

            // az AI-nak eleg egy label matrix -> ha az AI lő akkor simán csak az own courttel kell összevetni, h volt e talalat

        }

        bool checkIfAllShipIsPlaced(Label[,] court)
        {
            int shipCounter = 0;
            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < ColumnCount; j++)
                {
                    if (court[i, j].BackColor == Color.Black)
                    {
                        shipCounter++;
                    }
                }
            }
            if (shipCounter == 16) // 16 label fekete ha az összes hajót elhelyeztük a pályán
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        bool checkIfAllShipIsOff(Label[,] court)
        {
            int shipcounter = 0;
            for(int i = 0; i < RowCount; i++)
            {
                for(int j = 0; j < ColumnCount; j++)
                {
                    if(court[i,j].BackColor == Color.Red)
                    {
                        shipcounter++;
                    }
                }
            }
            if(shipcounter == 16)
            {
                return true;
            }else
            {
                return false;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Really Quit?", "Exit", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void btReadyForStart_Click(object sender, EventArgs e)
        {
            if (checkIfAllShipIsPlaced(ownCourt)){
                MessageBox.Show("A hajók pozicióba álltak, ha készen állsz mehet a csata!");
                btReadyForStart.Enabled = false;
                //bool AIoff = false;
                while ((!checkIfAllShipIsOff(AICourt)) && (!checkIfAllShipIsOff(ownCourt)) ) // addig kene menni amig vki nem nyert
                {
                    playerTurn();
                    AITurn();
                    if (AIflag)
                    {
                        while (AIflag)
                        {
                            AIflag = false;
                            Thread.Sleep(1000);
                            AITurn();
                        }
                    }
                }
                if (checkIfAllShipIsOff(AICourt))
                {
                    MessageBox.Show("A csatának vége.Nyertél! Gratulálunk!");
                    clearTable(AICourt);
                    clearTable(ownCourt);
                    clearTable(enemyCourt);
                    btReadyForStart.Enabled = true;
                    setShipsToDefault();
                    
                }
                else if (checkIfAllShipIsOff(ownCourt))
                {
                    MessageBox.Show("A csatának vége. Sajnos vesztettél... :(");
                    clearTable(AICourt);
                    clearTable(ownCourt);
                    clearTable(enemyCourt);
                    btReadyForStart.Enabled = true;
                    setShipsToDefault();
                }
            }else
            {
                MessageBox.Show("Helyezd el az összes hajót!");
                
            }
        }

        void playerTurn()
        {
            
            while (!playerIsShooted)
            {
                Thread.Sleep(250); // pause for 1/4 second;
                Application.DoEvents();
            };
        }


        void clearTable(Label[,] court)
        {
            for (int i = 0; i < RowCount; i++)
            {
                for (int j = 0; j < ColumnCount; j++)
                {
                    court[i, j].BackColor = Color.LightBlue;
                    court[i, j].Text = "";
                }
            }
        }



        void decodeAddStack(string components)
        {
            //MessageBox.Show(components);
            float tort = (float.Parse(components) * 10);
            //MessageBox.Show(tort.ToString());
            int dsa = (int)tort;
            nai.addNeighboursToStack(dsa);
        }

        int AICelpont = 0;

        //MessageBox.Show("AICelpont: " + AICelpont);
        int i, j = 0;
        double atalakitas = 0.0;
        string szoveg = "";
        string[] ij;
        bool AIflag = false;

        private void normalModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            nai = new NormalAI();
            initializeAICourt();
            for (int i = 0; i < ships.Length; i++)
            {
                ships[i].Visible = true;
            }
            MessageBox.Show("A játék megkezdéséhez, helyezd el a hajóidat a pályán, majd nyomj a Mehet! gombra.");
        }

        void AITurn()
        {           
            playerIsShooted = false;

            AICelpont = nai.fire(RowCount, ColumnCount);

            //Generált random szám leképzése a táblára
            //0-99 megszámozzuk a tábla mezőit
            if (AICelpont == 100)
            {
                atalakitas = ((double)AICelpont - 1) / 10; // =9,9 -> külön kell szedni [9,9]-re, ezert átalakitom stringbe és tokenizálom
                //MessageBox.Show("atalakitas utan: " + atalakitas);
                szoveg = atalakitas.ToString();
                ij = szoveg.Split(',');

                j = Int32.Parse(ij[0]);
                i = Int32.Parse(ij[1]);
                
                //MessageBox.Show("i: " + i.ToString());
                //MessageBox.Show("j: " + j.ToString());
            }
            else if (AICelpont == 0)
            {
                i = 0;
                j = 0;
            }
            else if ((AICelpont % 10) == 0) 
            {
                //10,20,30,40,50,60,70,80,90  0,9
                atalakitas = (double)AICelpont / 100;
                //szoveg = atalakitas.ToString();
                //ij = szoveg.Split(',');
                ij = (atalakitas.ToString()).Split(',');

                i = Int32.Parse(ij[0]);
                j = Int32.Parse(ij[1]);
                //MessageBox.Show("i: " + i.ToString());
                //MessageBox.Show("j: " + j.ToString());
            }
            else
            {
                atalakitas = ((double)AICelpont / 10); //pl 1 -> 0,1, külön szedem [0,1] 
                //MessageBox.Show("atalakitas utan: " + atalakitas);
                //szoveg = atalakitas.ToString();
                //ij = szoveg.Split(',');
                Console.WriteLine(atalakitas);
                ij = (atalakitas.ToString()).Split(','); //ezel van baja!!!
                j = Int32.Parse(ij[0]);
                i = Int32.Parse(ij[1]); //néha itt indexoutofbound exception?!?!

                //MessageBox.Show("i: " + i.ToString());
                //MessageBox.Show("j: " + j.ToString());
            }

            //************ellenorzom hogy a gép hova lött*************
            if(ownCourt[i,j].BackColor == Color.Black)
            {
                //a gep eltalalta a hajóm
                ownCourt[i, j].BackColor = Color.Red;
                ownCourt[i, j].Text = "X";
                ownSunkShipCounter++;
                AIIsAlreadyShooted = true;
                nai.IsHuntingFlag = true;
                //hunt/target szomszédok felvétele a stackbe
                //
                //legjobb esetben: [i,j-1] [i,j+1] [i-1,j] [i+1,j]

                if (i == 0 && j == 0)
                {
                    //bal felso sarok
                    //i+1,j
                    //i, j+1
                    nai.addNeighboursToStack(1);
                    nai.addNeighboursToStack(10);

                }
                else if (i == 9 && j == 0)
                {
                    //jobb felso sarok!!!
                    nai.addNeighboursToStack(8);
                    nai.addNeighboursToStack(19);
                }
                else if (i == 0 && j == 9)
                {
                    //bal also sarok!!!
                    nai.addNeighboursToStack(80);
                    nai.addNeighboursToStack(91);

                }
                else if (i == 9 && j == 9)
                {
                    //jobb also sarok
                    //i-1,j
                    //i,j-1
                    nai.addNeighboursToStack(98);
                    nai.addNeighboursToStack(89);
                }
                else if (i == 0)
                {
                    //elso oszlop!!!
                    //i,j-1
                    //i+1,j
                    //i,j+1
                    string temp = (j-1) + "," + (i);
                    decodeAddStack(temp);

                    temp = (j) + "," + (i+1);
                    decodeAddStack(temp);
                   
                    temp = (j+1) + "," + (i);
                    decodeAddStack(temp);
                }
                else if( i == 9)
                {
                    //utolso oszlop!!!
                    //i,j-1
                    //i-1,j
                    //i,j+1
                    string temp = (j - 1) + "," + (i);
                    decodeAddStack(temp);

                    temp = (j) + "," + (i - 1);
                    decodeAddStack(temp);

                    temp = (j + 1) + "," + (i);
                    decodeAddStack(temp);
                }
                else if(j == 0)
                {
                    //elso sor!!! i j felcserelve -.- j=sor i = oszlop
                    //i-1,j
                    //i,j+1
                    //i+1,j
                    
                    string temp = j + "," +(i - 1);
                    decodeAddStack(temp);

                    temp =  (j + 1)+","+(i);
                    decodeAddStack(temp);

                    temp = (j)+","+(i+1);
                    decodeAddStack(temp);
                }
                else if(j == 9)
                {
                    //utolsó sor
                    //i-1,j
                    //i,j-1
                    //i+1,j
                   
                    string temp = j + "," + (i - 1);
                    decodeAddStack(temp);
                    
                    temp = (j - 1) + "," + (i);
                    decodeAddStack(temp);

                    temp = (j) + "," + (i + 1);
                    decodeAddStack(temp);
                }
                else
                {
                    //rendes helyen van, kell az osszes szomszed
                    //i,j-1
                    //i,j+1
                    //i-1,j
                    //i+1,j
                    
                    string temp = (j-1) + "," + (i);
                    decodeAddStack(temp);
                    
                    temp = (j + 1) + "," + (i);
                    decodeAddStack(temp);
                  
                    temp = (j) + "," + (i - 1);
                    decodeAddStack(temp);
                   
                    temp = (j) + "," + (i + 1);
                    decodeAddStack(temp);
                }
            }
            else if(ownCourt[i,j].BackColor == Color.WhiteSmoke || ownCourt[i,j].BackColor == Color.Red)
            {
                //a gep ugyanoda lott ahova mar elozoleg
                //MessageBox.Show("Ide mar lott a gep...");
                //nai.IsHuntingFlag = true; // ezt majd ki kell venni innen
                //AITurn();
                if (nai.isHuntingFlag)
                {
                    AITurn();
                }
                else {
                    AIflag = true;
                }
            }
            else
            {
                // a gep melle lott
                ownCourt[i, j].BackColor = Color.WhiteSmoke;
                ownCourt[i, j].Text = "O";
                //nai.IsHuntingFlag = true; // ezt majd ki kell venni innen
                AIIsAlreadyShooted = true;
            }

            if (AIIsAlreadyShooted)
            {
                MessageBox.Show("A gép lőtt...ismét te jösz :)");
                AIIsAlreadyShooted = false;
            }

        }

        private void lbl_Click(object sender, System.EventArgs e)
        {
            Label lbl = (Label)sender;
            Color color = lbl.BackColor;
            if (color == Color.WhiteSmoke)
            {
                playerIsShooted = false;
                MessageBox.Show("Oda már lőttél!");
            }
            else
            {
                playerIsShooted = true;
                
                for (int i = 0; i < RowCount; i++)
                {
                    for (int j = 0; j < ColumnCount; j++) {
                        /*if (lbl.Location == enemyCourt[i, j].Location) //igy tudom a listaban is frissiteni az infot és nem csak megjelenesben amire kattintottam?!
                        {
                            color = System.Drawing.Color.DarkRed; //loves
                            enemyCourt[i, j].BackColor = Color.DarkRed;
                            //MessageBox.Show(enemyCourt[i, j].BackColor.ToString());
                        }*/

                        if(lbl.Location == enemyCourt[i, j].Location)
                        {
                            if (AICourt[i, j].BackColor == Color.Black)
                            {
                                //MessageBox.Show("talalat!");
                                //ownCourt[i, j].BackColor = Color.Red; //talalat jelzes own listaban is
                                enemyCourt[i, j].BackColor = Color.Red;
                                lbl.Text = "X"; // talalat jelzes enemy oldalon
                                AISunkShipCounter++;
                                color = Color.Red; // talalat jelzes enemy oldalon
                                //enemyCourt[i, j].BackColor = Color.Red; //enemy listaban is update-eljuk a szint
                                AICourt[i, j].BackColor = Color.Red;
                            }
                            else
                            {
                                //MessageBox.Show("sajnos melle ment :(");
                                //ownCourt[i, j].BackColor = Color.WhiteSmoke; //melle ment jelzes own listaban
                                AICourt[i, j].BackColor = Color.WhiteSmoke;
                                lbl.Text = "O"; // melle ment jelzes enemy oldalon
                                color = Color.WhiteSmoke; // melle ment jelzes enemy oldalon
                            }
                        }
                    }
                }
            }
            lbl.BackColor = color;
        }
        /****************************************/
    }
}
