﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShipApplication
{
    class NormalAI : CoreAI
    {
        //bool isHuntingFlag; //ture - Hunt mode, false - Target mode  
        //List<int> stack = null;

        public NormalAI()
        {
            isHuntingFlag = false;
            stack = new List<int>();
        }

        override public int fire(int rowNumber, int columNumber)
        {
            if (IsHuntingFlag)
            {
                if (stack.Count != 0)
                {
                    int next = stack.First();
                    stack.RemoveAt(0);
                    return next;
                } else
                {
                    IsHuntingFlag = false;
                    return (int)(new Random().NextDouble() * rowNumber * columNumber);
                }
            } else
            {
                return (int)(new Random().NextDouble() * rowNumber * columNumber);
            }
        }

        override public void addNeighboursToStack(int neighbour)
        {
            stack.Add(neighbour);
        }
    }
}
