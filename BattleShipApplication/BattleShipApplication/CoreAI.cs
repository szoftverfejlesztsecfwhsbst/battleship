﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShipApplication
{
    class CoreAI
    {
        public bool isHuntingFlag;
        public List<int> stack = null;

        public bool IsHuntingFlag { get; internal set; }

        public virtual int fire(int rowNumber, int columNumber)
        {
            return (int)( new Random().NextDouble() * rowNumber * columNumber);
        }

        public virtual void addNeighboursToStack(int neighbour)
        {
            
        }
    }
}
