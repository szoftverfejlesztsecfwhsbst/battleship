﻿namespace BattleShipApplication
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.singlePlayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.lbServerAddress = new System.Windows.Forms.Label();
            this.lbIpPort = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.connectionProperties = new System.Windows.Forms.GroupBox();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.tbIPAddress = new System.Windows.Forms.TextBox();
            this.lbPort = new System.Windows.Forms.Label();
            this.lbIPAddress = new System.Windows.Forms.Label();
            this.btnJoin = new System.Windows.Forms.Button();
            this.serverAddress = new System.Windows.Forms.GroupBox();
            this.btReadyForStart = new System.Windows.Forms.Button();
            this.normalModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.connectionProperties.SuspendLayout();
            this.serverAddress.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.singlePlayerToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(895, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // singlePlayerToolStripMenuItem
            // 
            this.singlePlayerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newGameToolStripMenuItem,
            this.normalModeToolStripMenuItem});
            this.singlePlayerToolStripMenuItem.Name = "singlePlayerToolStripMenuItem";
            this.singlePlayerToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.singlePlayerToolStripMenuItem.Text = "Single player";
            // 
            // newGameToolStripMenuItem
            // 
            this.newGameToolStripMenuItem.Name = "newGameToolStripMenuItem";
            this.newGameToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.newGameToolStripMenuItem.Text = "Easy Mode";
            this.newGameToolStripMenuItem.Click += new System.EventHandler(this.newGameToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // lbServerAddress
            // 
            this.lbServerAddress.AutoSize = true;
            this.lbServerAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbServerAddress.Location = new System.Drawing.Point(19, 30);
            this.lbServerAddress.Name = "lbServerAddress";
            this.lbServerAddress.Size = new System.Drawing.Size(175, 25);
            this.lbServerAddress.TabIndex = 2;
            this.lbServerAddress.Text = "Server IP and port:";
            // 
            // lbIpPort
            // 
            this.lbIpPort.AutoSize = true;
            this.lbIpPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbIpPort.Location = new System.Drawing.Point(715, 287);
            this.lbIpPort.Name = "lbIpPort";
            this.lbIpPort.Size = new System.Drawing.Size(0, 24);
            this.lbIpPort.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(715, 287);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 24);
            this.label1.TabIndex = 5;
            // 
            // connectionProperties
            // 
            this.connectionProperties.Controls.Add(this.tbPort);
            this.connectionProperties.Controls.Add(this.tbIPAddress);
            this.connectionProperties.Controls.Add(this.lbPort);
            this.connectionProperties.Controls.Add(this.lbIPAddress);
            this.connectionProperties.Controls.Add(this.btnJoin);
            this.connectionProperties.Location = new System.Drawing.Point(12, 27);
            this.connectionProperties.Name = "connectionProperties";
            this.connectionProperties.Size = new System.Drawing.Size(200, 134);
            this.connectionProperties.TabIndex = 6;
            this.connectionProperties.TabStop = false;
            this.connectionProperties.Text = "ConnectionProperties";
            // 
            // tbPort
            // 
            this.tbPort.Location = new System.Drawing.Point(73, 57);
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(100, 20);
            this.tbPort.TabIndex = 4;
            // 
            // tbIPAddress
            // 
            this.tbIPAddress.Location = new System.Drawing.Point(73, 27);
            this.tbIPAddress.Name = "tbIPAddress";
            this.tbIPAddress.Size = new System.Drawing.Size(100, 20);
            this.tbIPAddress.TabIndex = 3;
            // 
            // lbPort
            // 
            this.lbPort.AutoSize = true;
            this.lbPort.Location = new System.Drawing.Point(6, 57);
            this.lbPort.Name = "lbPort";
            this.lbPort.Size = new System.Drawing.Size(29, 13);
            this.lbPort.TabIndex = 2;
            this.lbPort.Text = "Port:";
            // 
            // lbIPAddress
            // 
            this.lbIPAddress.AutoSize = true;
            this.lbIPAddress.Location = new System.Drawing.Point(6, 30);
            this.lbIPAddress.Name = "lbIPAddress";
            this.lbIPAddress.Size = new System.Drawing.Size(61, 13);
            this.lbIPAddress.TabIndex = 1;
            this.lbIPAddress.Text = "IP Address:";
            // 
            // btnJoin
            // 
            this.btnJoin.Location = new System.Drawing.Point(42, 95);
            this.btnJoin.Name = "btnJoin";
            this.btnJoin.Size = new System.Drawing.Size(75, 23);
            this.btnJoin.TabIndex = 0;
            this.btnJoin.Text = "Join";
            this.btnJoin.UseVisualStyleBackColor = true;
            this.btnJoin.Click += new System.EventHandler(this.btnJoin_Click);
            // 
            // serverAddress
            // 
            this.serverAddress.Controls.Add(this.lbServerAddress);
            this.serverAddress.Location = new System.Drawing.Point(683, 328);
            this.serverAddress.Name = "serverAddress";
            this.serverAddress.Size = new System.Drawing.Size(200, 80);
            this.serverAddress.TabIndex = 7;
            this.serverAddress.TabStop = false;
            this.serverAddress.Text = "Server Address";
            // 
            // btReadyForStart
            // 
            this.btReadyForStart.Location = new System.Drawing.Point(380, 384);
            this.btReadyForStart.Name = "btReadyForStart";
            this.btReadyForStart.Size = new System.Drawing.Size(75, 23);
            this.btReadyForStart.TabIndex = 8;
            this.btReadyForStart.Text = "Mehet!";
            this.btReadyForStart.UseVisualStyleBackColor = true;
            this.btReadyForStart.Click += new System.EventHandler(this.btReadyForStart_Click);
            // 
            // normalModeToolStripMenuItem
            // 
            this.normalModeToolStripMenuItem.Name = "normalModeToolStripMenuItem";
            this.normalModeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.normalModeToolStripMenuItem.Text = "Normal Mode";
            this.normalModeToolStripMenuItem.Click += new System.EventHandler(this.normalModeToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 420);
            this.Controls.Add(this.btReadyForStart);
            this.Controls.Add(this.serverAddress);
            this.Controls.Add(this.connectionProperties);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbIpPort);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.connectionProperties.ResumeLayout(false);
            this.connectionProperties.PerformLayout();
            this.serverAddress.ResumeLayout(false);
            this.serverAddress.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem singlePlayerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newGameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label lbServerAddress;
        private System.Windows.Forms.Label lbIpPort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox connectionProperties;
        private System.Windows.Forms.Button btnJoin;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.TextBox tbIPAddress;
        private System.Windows.Forms.Label lbPort;
        private System.Windows.Forms.Label lbIPAddress;
        private System.Windows.Forms.GroupBox serverAddress;
        private System.Windows.Forms.Button btReadyForStart;
        private System.Windows.Forms.ToolStripMenuItem normalModeToolStripMenuItem;
    }
}

